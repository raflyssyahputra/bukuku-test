<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\KursController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [KursController::class, 'scrape']);
Route::post('/store-kurs', [KursController::class, 'storeKurs']);
Route::post('/save-storage', [KursController::class, 'storeKursToStorage']);
Route::post('/delete-storage', [KursController::class, 'deleteStorage']);

