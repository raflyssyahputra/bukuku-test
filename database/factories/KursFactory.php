<?php

namespace Database\Factories;

use App\Models\Kurs;
use Illuminate\Database\Eloquent\Factories\Factory;

class KursFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Kurs::class;
    
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'local_date' => $this->faker->date,
            'world_date' => $this->faker->date,
            'kurs' => $this->faker->text,
        ];
    }
}
