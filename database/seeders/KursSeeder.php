<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Kurs;

class KursSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kurs::factory()->times(50)->create();
    }
}
