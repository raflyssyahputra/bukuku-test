<?php

namespace App\Jobs;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Storage; 
use Illuminate\Support\Facades\Redis;


class DeleteFileJob implements ShouldQueue 
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function handle()
    {
        $redist = Redis::get('list-file');
        $files = explode(', ', $redist);
        foreach ($files as $file) {
            if (Storage::path($file)) {
                Storage::delete($file);
            }
        }
    }
}