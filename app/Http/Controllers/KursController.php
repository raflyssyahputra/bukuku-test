<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Scrapers\ScrapeKurs;
use App\Interfaces\KursRepositoryInterface;
use App\Services\KursService;
use App\Jobs\DeleteFileJob;
use Illuminate\Support\Facades\Storage; 
use Illuminate\Support\Facades\Redis;


class KursController extends Controller
{
    private $kursService;

    public function __construct(KursService $kursService)
    {
        $this->kursService = $kursService;
    }

    public function scrape()
    {
        $path = 'public/';
        $data = Storage::allFiles($path);
        return view('scraped-data', ['data' => $data]);
    }

    public function storeKurs()
    {
        $url = 'https://kursdollar.org/';
        $scraper = new ScrapeKurs();

        try {
            $data = $this->scrapeWithRetries($scraper, $url);
            $kursData = $this->kursService->storeKursScraper($data);
            return $kursData;
        } catch (\Exception $e) {
            return ['error' => 'Failed to scrape data after multiple attempts.'];
        }
    }

    private function scrapeWithRetries($scraper, $url, $maxRetries = 3, $retryCount = 0)
    {
        try {
            return $scraper->scrapeData($url);
        } catch (\Exception $e) {
            if ($retryCount < $maxRetries) {

                usleep(500000);

                return $this->scrapeWithRetries($scraper, $url, $maxRetries, ++$retryCount);
            } else {
                throw $e;
            }
        }
    }

    public function storeKursToStorage()
    {
        $url = 'https://kursdollar.org/';
        $scraper = new ScrapeKurs();
        try {
            $data = $this->scrapeWithRetries($scraper, $url);
            $kursData = $this->kursService->storeKursToStorage($data);
            return $data;
        } catch (\Exception $e) {
            return ['error' => 'Failed to scrape data after multiple attempts.'];
        }
    }

    public function deleteStorage()
    {

        $path = 'public/';
        $files = Storage::allFiles($path);
        if(count($files) == 0){
            return redirect()->back()->with('success', 'File Kosong!');
        }
        $result = implode(', ', $files);

        try {
            Redis::set('list-file', $result);
            DeleteFileJob::dispatch();

            return redirect()->back()->with('success', 'Proses berhasil ditambahkan pada Job!');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
