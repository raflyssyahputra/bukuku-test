<?php

namespace App\Repositories;

use App\Interfaces\KursRepositoryInterface;
use App\Models\Kurs;

class KursRepository implements KursRepositoryInterface 
{
    public function getAllKurses() 
    {
        return Kurs::all();
    }

    public function deleteKurs($KursId) 
    {
        Kurs::destroy($KursId);
    }

    public function createKurs(array $KursDetails) 
    {
        return Kurs::create($KursDetails);
    }

    public function updateKurs($KursId, array $newDetails) 
    {
        return Kurs::whereId($KursId)->update($newDetails);
    }

    public function getFulfilledKurss() 
    {
        return Kurs::where('is_fulfilled', true);
    }
}
