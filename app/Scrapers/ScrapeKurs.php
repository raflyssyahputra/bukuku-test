<?php

namespace App\Scrapers;

use Goutte\Client;
use Carbon\Carbon;

class ScrapeKurs
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function scrapeData($url)
    {
        try {
            $crawler = $this->client->request('GET', $url);

            $tableMeta = $this->getMeta($crawler);
            $tableKurs = $this->getKurs($crawler);

            $returnObjct = (object)[
                "meta" => $tableMeta[0],
                "kurs" => $tableKurs
            ];

            return $returnObjct;
        } catch (\Exception $e) {
            // Handle the exception as per your application's requirements
            // You can log the error, send an alert, or return a default response
            return (object) [
                "error" => "An error occurred during scraping: " . $e->getMessage()
            ];
        }
    }

    private function getMeta($crawler)
    {
        
        return $crawler->filter('table.in_table tr.title_table')->each(function ($row) {
            $indo = $row->filter('td:nth-child(2)')->text();
            $dunia = $row->filter('td:nth-child(3)')->text();
            $currentDate = Carbon::now('UTC')->addHours(7);
            $date = $currentDate->format('d-m-Y');
            $day = $currentDate->format('l');
            return [
                'date'=> $date,
                'day'=> $day,
                'indonesia' => $indo,
                'word' => $dunia,
            ];
        });

    }

    private function getKurs($crawler)
    {
        return $crawler->filter('table.in_table tr[id^="tr_id_"]')->each(function ($row) {
            $currency = str_replace(['↓', '↑', "\xc2\xa0"], '', $row->filter('td:first-child strong')->text());

            $buy = $row->filter('td:nth-child(2)')->text();
            preg_match('/\(([^)]+)\)/', $buy, $matches);
            $buy = isset($matches[0]) ? str_replace($matches[0], '', $buy) : $buy;

            $sell = $row->filter('td:nth-child(2)')->text();
            preg_match('/\(([^)]+)\)/', $sell, $matches);
            $sell = isset($matches[0]) ? str_replace($matches[0], '', $sell) : $sell;

            $average = $row->filter('td:nth-child(2)')->text();
            preg_match('/\(([^)]+)\)/', $average, $matches);
            $average = isset($matches[0]) ? str_replace($matches[0], '', $average) : $average;

            $word_rate = $row->filter('td:nth-child(5)')->text();

            return (object)[
                'currency' => $currency,
                'buy' => trim($buy),
                'sell' => trim($sell),
                'average' => trim($average),
                'word_rate' => $word_rate,
            ];
        });

    }

}