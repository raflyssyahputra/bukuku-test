<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\KursController;

class GetKursCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kurs:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get kurs every 7 Minutes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(KursController $kursController)
    {
        parent::__construct();
        $this->kursController = $kursController;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $this->kursController->storeKurs();
            $this->info('Task Get Kurs executed successfully!');
            \Log::info('Task Get Kurs executed successfully!');
        } catch (\Exception $e) {
            $this->error("Error: {$e->getMessage()}");
            \Log::error("Error: {$e->getMessage()}");
        }
    }
}
