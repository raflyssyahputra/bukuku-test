<?php

namespace App\Services;
use App\Interfaces\KursRepositoryInterface;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class KursService
{

    private KursRepositoryInterface $kursRepository;

    public function __construct(KursRepositoryInterface $kursRepository) 
    {
        $this->kursRepository = $kursRepository;
    }

    public function storeKursScraper($data)
    {
        try {
            $meta = $data->meta;
            $kursArray = $data->kurs;

            $kursDetails = [
                'local_date' => $meta["indonesia"],
                'world_date' => $meta["word"],
                'kurs' => Crypt::encryptString(json_encode($kursArray)),
            ];

            $kurs = $this->kursRepository->createKurs($kursDetails);

            return $kurs;
        } catch (\Exception $e) {
            return ['error' => 'Failed to store kurs data.'];
        }
    }

    public function storeKursToStorage($data)
    {
        try {
            $now = Carbon::now('UTC')->addHours(7);
            $dateTimeFormatted = $now->format('d-m-Y--H-i-s');
            $filename = "rate-$dateTimeFormatted.json";
            $path = "public/{$filename}";
            $jsonString = json_encode($data, JSON_PRETTY_PRINT);
            Storage::put($path, $jsonString);
            return $data;
        } catch (\Exception $e) {
            return ['error' => 'Failed to Save Json Data.'];
        }
    }

}
