<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\KursService;
use App\Interfaces\KursRepositoryInterface;
use App\Repositories\KursRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     */
    public function register()
    {
        $this->app->bind(KursService::class, function ($app) {
            return new KursService($app->make(KursRepositoryInterface::class));
        });
        $this->app->bind(KursRepositoryInterface::class, KursRepository::class);

    }

    /**
     * Bootstrap any application services.
     *
     */
    public function boot()
    {
        //
    }
}
