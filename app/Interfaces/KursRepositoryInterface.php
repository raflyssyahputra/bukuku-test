<?php

namespace App\Interfaces;

interface KursRepositoryInterface 
{
    public function getAllKurses();
    public function deleteKurs($KursId);
    public function createKurs(array $KursDetails);
    public function updateKurs($KursId, array $newDetails);
    // public function getFulfilledKurses();
}
