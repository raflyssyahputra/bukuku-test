<!-- resources/views/dollar-rates.blade.php -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dollar Rates</title>
</head>
<body>
    <h1>List Storage File Kurs</h1>

    <form action="{{ url('/delete-storage') }}" method="POST">
        @csrf
        <button type="submit">Delete Storage</button>
    </form>

    <ul>
        @foreach($data as $file)
            <li>{{ $file }}</li>
        @endforeach
    </ul>

      @if (session('success'))
       <script>
           alert("{{ session('success') }}");
       </script>
   @endif

</body>
</html>
